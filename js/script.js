$(document).ready(function(){
    // Activate Carousel
    $("#myCarousel").carousel({interval: 800, wrap: true});
    $("form[name='registration']").validate({
    rules: {
      	email: {
        	required: true,
        	email: true
      	},
      	phone: {
        	required: true,
        	minlength: 10
      	}
    },
    messages: {
      	phone: {
        	required: "Please provide a phone",
       		minlength: "Your phone must be at least 10 number long"
      	},
      email: "Please enter a valid email address"
    },
    submitHandler: function(form) {
      form.submit();
      $('#valid-issue').html('<div class="alert alert-success alert-message">Your message has been sent,<BR> We will contact you back with in next 24 hours.</div>').show(300);
    }
  });
});